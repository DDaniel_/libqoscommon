/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_QUEUE_STATS_H__)
#define __QOS_QUEUE_STATS_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @defgroup Queue_Stats 4. The queue-stats type
 * @brief
 * This library provides for the QoS.QueueStats object a typed wrapper and functions to facilitate its use.
 * The type provided is qos_queue_stats_t.
 *
 * The functions help for memory management during construction and destruction, as well as get and set data.
 *  - Actions of creation and destruction don't reflect on the data-model.
 *  - Actions of data manipulations are instantaneously repercuted on the data-model (no notifications triggered)
 */

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

#include "qos-type.h"

#define QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH "QoS.QueueStats"
#define QOS_TR181_DEVICE_QOS_QUEUE_STATS_INSTANCE "QoS.QueueStats."
#define QOS_TR181_DEVICE_QOS_QUEUE_STATS_OBJECT_NAME "QueueStats"

struct _qos_node;
struct _netmodel_query;

/**
 * @ingroup Queue_Stats
 * @brief
 * Definition of the qos_queue_stats_t.
 */
typedef struct _qos_queue_stats {
    amxd_object_t* dm_object;      /**< The data model object */
    struct _qos_node* node;        /**< A pointer to the node object */
    char* intf_name;               /**< The interface name given by libnetmodel */
    struct _netmodel_query* query; /**< The interface query for update */
} qos_queue_stats_t;

/**
 * @ingroup Queue_Stats
 * @brief
 * Return the data-model object.
 *
 * /!\ This function breaks the encapsulation and should be use cautiously.
 */
amxd_object_t* qos_queue_stats_get_dm_object(const qos_queue_stats_t* const queue_stats);

/**
 * @ingroup Queue_Stats
 * @brief
 * Allocate and initiate a qos_queue_stats_t object with an instance from the data-model.
 */
int qos_queue_stats_new(qos_queue_stats_t** queue_stats, amxd_object_t* const queue_stats_instance);

/**
 * @ingroup Queue_Stats
 * @brief
 * Destroy a qos_queue_stats_t object previously allocated with qos_queue_stats_new and set the pointer to NULL.
 *
 * /!\ This function doesn't destroy the data-model in itself nor the node, only the wrapper.
 */
int qos_queue_stats_delete(qos_queue_stats_t** queue_stats);

/**
 * @ingroup Queue_Stats
 * @brief
 * Initiate a qos_queue_stats_t object previously allocated.
 */
int qos_queue_stats_init(qos_queue_stats_t* const queue_stats, amxd_object_t* const queue_stats_instance);

/**
 * @ingroup Queue_Stats
 * @brief
 * Deinitiate a qos_queue_stats_t object.
 */
int qos_queue_stats_deinit(qos_queue_stats_t* const queue_stats);

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model Status parameter value and return it.
 */
qos_status_t qos_queue_stats_dm_get_status(const qos_queue_stats_t* const queue_stats);

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model Enable parameter value and return it.
 */
static inline bool qos_queue_stats_dm_get_enable(const qos_queue_stats_t* const stats) {
    return stats == NULL ? false : amxd_object_get_value(bool, stats->dm_object, "Enable", NULL);
}

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model Interface parameter value and return it.
 */
static inline const char* qos_queue_stats_dm_get_interface(const qos_queue_stats_t* const stats) {
    return stats == NULL ? NULL : stats->intf_name;
}

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model Queue parameter value and return it.
 */
static inline const char* qos_queue_stats_dm_get_queue(const qos_queue_stats_t* const stats) {
    return stats == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(stats->dm_object, "Queue"));
}

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model Status parameter and change its value to the status argument.
 */
int qos_queue_stats_dm_set_status(const qos_queue_stats_t* const queue_stats, const qos_status_t status);

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model OutputPackets parameter value and return it.
 */
static inline uint32_t qos_queue_stats_dm_get_output_packets(const qos_queue_stats_t* const stats) {
    return stats == NULL ? 0 : amxc_var_constcast(uint32_t, amxd_object_get_param_value(stats->dm_object, "OutputPackets"));
}

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model OutputBytes parameter value and return it.
 */
static inline uint32_t qos_queue_stats_dm_get_output_bytes(const qos_queue_stats_t* const stats) {
    return stats == NULL ? 0 : amxc_var_constcast(uint32_t, amxd_object_get_param_value(stats->dm_object, "OutputBytes"));
}

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model DroppedPackets parameter value and return it.
 */
static inline uint32_t qos_queue_stats_dm_get_dropped_packets(const qos_queue_stats_t* const stats) {
    return stats == NULL ? 0 : amxc_var_constcast(uint32_t, amxd_object_get_param_value(stats->dm_object, "DroppedPackets"));
}

/**
 * @ingroup Queue_Stats
 * @brief
 * Access data-model DroppedBytes parameter value and return it.
 */
static inline uint32_t qos_queue_stats_dm_get_dropped_bytes(const qos_queue_stats_t* const stats) {
    return stats == NULL ? 0 : amxc_var_constcast(uint32_t, amxd_object_get_param_value(stats->dm_object, "DroppedBytes"));
}

#ifdef __cplusplus
}
#endif

#endif // __QOS_QUEUE_STATS_H__
