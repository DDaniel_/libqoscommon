/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_SCHEDULER_H__)
#define __QOS_SCHEDULER_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @defgroup Scheduler 2. The scheduler type
 * @brief
 * This library provides for the QoS.Scheduler object a typed wrapper and functions to facilitate its use.
 * The type provided is qos_scheduler_t.
 *
 * The functions help for memory management during construction and destruction, as well as get and set data.
 *  - Actions of creation and destruction don't reflect on the data-model.
 *  - Actions of data manipulations are instantaneously repercuted on the data-model (no notifications triggered)
 */

#include <stdint.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>

#include "qos-type.h"

#define QOS_TR181_DEVICE_QOS_SCHEDULER_PATH "QoS.Scheduler"
#define QOS_TR181_DEVICE_QOS_SCHEDULER_INSTANCE "QoS.Scheduler."
#define QOS_TR181_DEVICE_QOS_SCHEDULER_OBJECT_NAME "Scheduler"

struct _qos_node;
struct _netmodel_query;

/**
 * @ingroup Scheduler
 * @brief
 * Definition of the qos_scheduler_t.
 */
typedef struct _qos_scheduler {
    amxd_object_t* dm_object;      /**< The data model object */
    struct _qos_node* node;        /**< A pointer to the node object */
    char* intf_name;               /**< The interface name given by libnetmodel */
    struct _netmodel_query* query; /**< The interface query for update */
} qos_scheduler_t;

/**
 * @ingroup Scheduler
 * @brief
 * Return the data-model object.
 *
 * /!\ This function breaks the encapsulation and should be use cautiously.
 */
amxd_object_t* qos_scheduler_get_dm_object(const qos_scheduler_t* const scheduler);

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model Status parameter value and return it.
 */
qos_status_t qos_scheduler_dm_get_status(const qos_scheduler_t* const scheduler);

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model Alias parameter value and return it.
 */
static inline const char* qos_scheduler_dm_get_alias(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(scheduler->dm_object, "Alias"));
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model Interface parameter value and return it.
 */
static inline const char* qos_scheduler_dm_get_interface(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? NULL : scheduler->intf_name;
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model DefaultQueue parameter value and return it.
 */
static inline const char* qos_scheduler_dm_get_default_queue(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? NULL : amxc_var_constcast(cstring_t, amxd_object_get_param_value(scheduler->dm_object, "DefaultQueue"));
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model Enable parameter value and return it.
 */
static inline bool qos_scheduler_dm_get_enable(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? false : amxd_object_get_value(bool, scheduler->dm_object, "Enable", NULL);
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model ShapingRate parameter value and return it.
 */
static inline int32_t qos_scheduler_dm_get_shaping_rate(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? -1 : amxd_object_get_value(int32_t, scheduler->dm_object, "ShapingRate", NULL);
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model AssuredRate parameter value and return it.
 */
static inline int32_t qos_scheduler_dm_get_assured_rate(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? -1 : amxd_object_get_value(int32_t, scheduler->dm_object, "AssuredRate", NULL);
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model SchedulerAlgorithm parameter value and return it.
 */
qos_scheduler_algorithm_t qos_scheduler_dm_get_scheduler_algorithm(const qos_scheduler_t* const scheduler);

/**
 * @ingroup Scheduler
 * @brief
 * Return the instance's index in the QoS.Scheduler list.
 */
static inline uint32_t qos_scheduler_dm_get_index(const qos_scheduler_t* const scheduler) {
    return scheduler == NULL ? 0 : amxd_object_get_index(scheduler->dm_object);
}

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model Enable parameter and change its value to the enable argument.
 */
int qos_scheduler_dm_set_enable(const qos_scheduler_t* const scheduler,
                                const bool enable);
/**
 * @ingroup Scheduler
 * @brief
 * Access data-model Status parameter and change its value to the status argument.
 */
int qos_scheduler_dm_set_status(const qos_scheduler_t* const scheduler,
                                qos_status_t status);

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model ShapingRate parameter and change its value to the shaping_rate argument.
 */
int32_t qos_scheduler_dm_set_shaping_rate(const qos_scheduler_t* const scheduler,
                                          int32_t shaping_rate);

/**
 * @ingroup Scheduler
 * @brief
 * Access data-model AssuredRate parameter and change its value to the assured_rate argument.
 */
int32_t qos_scheduler_dm_set_assured_rate(const qos_scheduler_t* const scheduler,
                                          int32_t assured_rate);


/**
 * @ingroup Scheduler
 * @brief
 * Allocate and initiate a qos_scheduler_t object with an instance from the data-model.
 */
int qos_scheduler_new(qos_scheduler_t** scheduler, amxd_object_t* const x_scheduler_instance);

/**
 * @ingroup Scheduler
 * @brief
 * Destroy a qos_scheduler_t object previously allocated with qos_scheduler_new and set the pointer to NULL.
 *
 * /!\ This function doesn't destroy the data-model in itself nor the node, only the wrapper.
 */
int qos_scheduler_delete(qos_scheduler_t** scheduler);

/**
 * @ingroup Scheduler
 * @brief
 * Initiate a qos_scheduler_t object previously allocated.
 */
int qos_scheduler_init(qos_scheduler_t* const scheduler, amxd_object_t* const x_scheduler_instance);

/**
 * @ingroup Scheduler
 * @brief
 * Deinitiate a qos_scheduler_t object.
 */
int qos_scheduler_deinit(qos_scheduler_t* const scheduler);

#ifdef __cplusplus
}
#endif

#endif // __QOS_SCHEDULER_H__
