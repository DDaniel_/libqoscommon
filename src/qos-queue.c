/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "qos-assert.h"
#include "qos-queue.h"
#include "qos-type.h"

#define ME "queue"

int qos_queue_new(qos_queue_t** queue, amxd_object_t* queue_instance) {
    int retval = -1;
    when_null(queue, exit);
    when_null(queue_instance, exit);
    when_not_null(*queue, exit);
    when_not_null((qos_queue_t*) queue_instance->priv, exit);

    *queue = (qos_queue_t*) calloc(1, sizeof(qos_queue_t));
    when_null(*queue, exit);

    retval = qos_queue_init(*queue, queue_instance);
    when_failed(retval, failed);

exit:
    return retval;

failed:
    qos_queue_delete(queue);
    return retval;
}

int qos_queue_delete(qos_queue_t** queue) {
    int retval = -1;

    when_null(queue, exit);
    when_null(*queue, exit);

    qos_queue_deinit(*queue);

    free(*queue);
    *queue = NULL;
    retval = 0;

exit:
    return retval;
}

int qos_queue_init(qos_queue_t* const queue, amxd_object_t* const queue_instance) {
    int retval = -1;

    when_null(queue, exit);
    when_null(queue_instance, exit);
    memset(queue, 0, sizeof(qos_queue_t));
    queue_instance->priv = (void*) queue;
    queue->dm_object = queue_instance;

    retval = 0;

exit:
    return retval;
}

int qos_queue_deinit(qos_queue_t* const queue) {
    int retval = -1;

    when_null(queue, exit);

    queue->dm_object->priv = NULL;

    free(queue->intf_name);
    queue->intf_name = NULL;

    retval = 0;

exit:
    return retval;
}

amxd_object_t* qos_queue_get_dm_object(const qos_queue_t* const queue) {
    amxd_object_t* object = NULL;

    when_null(queue, exit);
    object = queue->dm_object;

exit:
    return object;

}

int qos_queue_dm_set_enable(const qos_queue_t* const queue, const bool enable) {
    int retval = -1;
    amxd_object_t* object = NULL;
    const char* param = "Enable";
    amxd_status_t status = amxd_status_ok;

    when_null(queue, exit);
    object = qos_queue_get_dm_object(queue);
    when_null(object, exit);

    status = amxd_object_set_value(bool, object, param, enable);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

qos_scheduler_algorithm_t qos_queue_dm_get_scheduler_algorithm(const qos_queue_t* const queue) {
    qos_scheduler_algorithm_t algo = QOS_SCHEDULER_ALGORITHM_SP;
    char* str_algo = NULL;

    when_null(queue, exit);

    str_algo = amxd_object_get_value(cstring_t, queue->dm_object, "SchedulerAlgorithm", NULL);
    when_str_empty(str_algo, exit);

    algo = qos_scheduler_algorithm_from_string(str_algo);
    free(str_algo);

exit:
    return algo;
}


qos_status_t qos_queue_dm_get_status(const qos_queue_t* const queue) {
    qos_status_t status = QOS_STATUS_DISABLED;
    char* str_status = NULL;

    when_null(queue, exit);

    str_status = amxd_object_get_value(cstring_t, queue->dm_object, "Status", NULL);
    when_str_empty(str_status, exit);

    status = qos_status_from_string(str_status);
    free(str_status);

exit:
    return status;
}

int qos_queue_dm_set_status(const qos_queue_t* const queue, qos_status_t status) {
    int retval = -1;
    const char* str_status = NULL;
    amxd_object_t* object = NULL;
    const char* param = "Status";
    amxd_status_t amx_status = amxd_status_ok;

    if(QOS_STATUS_LAST <= status) {
        goto exit;
    }

    when_null(queue, exit);
    object = qos_queue_get_dm_object(queue);
    when_null(object, exit);

    str_status = qos_status_to_string(status);
    amx_status = amxd_object_set_value(cstring_t, object, param, str_status);
    when_failed(amx_status, exit);

    retval = 0;

exit:
    return retval;
}

qos_queue_drop_algorithm_t qos_queue_dm_get_drop_algorithm(const qos_queue_t* const queue) {
    char* str_algorithm = NULL;
    qos_queue_drop_algorithm_t algorithm = QOS_QUEUE_DROP_ALGORITHM_RED;
    amxd_object_t* object = NULL;
    const char* param = "DropAlgorithm";

    when_null(queue, exit);
    object = qos_queue_get_dm_object(queue);
    when_null(object, exit);

    str_algorithm = amxd_object_get_value(cstring_t, object, param, NULL);
    when_str_empty(str_algorithm, exit);

    algorithm = qos_queue_drop_algorithm_from_string(str_algorithm);
    free(str_algorithm);

exit:
    return algorithm;
}

int qos_queue_dm_set_drop_algorithm(const qos_queue_t* const queue,
                                    const qos_queue_drop_algorithm_t algorithm) {

    int retval = -1;
    const char* str_algorithm = NULL;
    amxd_object_t* object = NULL;
    const char* param = "DropAlgorithm";
    amxd_status_t amx_status = amxd_status_ok;

    if(QOS_QUEUE_DROP_ALGORITHM_LAST <= algorithm) {
        goto exit;
    }

    when_null(queue, exit);
    object = qos_queue_get_dm_object(queue);
    when_null(object, exit);

    str_algorithm = qos_queue_drop_algorithm_to_string(algorithm);
    amx_status = amxd_object_set_value(cstring_t, object, param, str_algorithm);
    when_failed(amx_status, exit);

    retval = 0;

exit:
    return retval;
}

