/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "qos-type.h"
#include "qos-assert.h"

const char* qos_status_to_string(const qos_status_t status) {
    const char* str_status[] = {
        "Disabled", "Enabled", "Error_Misconfigured", "Error"
    };

    if(status >= QOS_STATUS_LAST) {
        return "Unknown";
    }
    return str_status[status];
}

qos_status_t qos_status_from_string(const char* status) {
    qos_status_t retval = QOS_STATUS_DISABLED;
    when_str_empty(status, exit);

    for(uint32_t i = 0; i < QOS_STATUS_LAST; i++) {
        const char* str_status = qos_status_to_string((qos_status_t) i);
        if(!strncmp(status, str_status, strlen(str_status))) {
            retval = (qos_status_t) i;
            break;
        }
    }
exit:
    return retval;
}

const char* qos_scheduler_algorithm_to_string(const qos_scheduler_algorithm_t algo) {
    const char* str_algo[] = {
        "WFQ", "WRR", "SP", "HTB", "SP_WRR", "DRR", "TBF", "FQ_CODEL",
    };

    if(algo >= QOS_SCHEDULER_ALGORITHM_LAST) {
        return "Unknown";
    }
    return str_algo[algo];
}

qos_scheduler_algorithm_t qos_scheduler_algorithm_from_string(const char* algo) {
    qos_scheduler_algorithm_t retval = QOS_SCHEDULER_ALGORITHM_SP;
    when_str_empty(algo, exit);


    for(uint32_t i = 0; i < QOS_SCHEDULER_ALGORITHM_LAST; i++) {
        const char* str_algo = qos_scheduler_algorithm_to_string((qos_scheduler_algorithm_t) i);
        if(!strncmp(algo, str_algo, strlen(str_algo))) {
            retval = (qos_scheduler_algorithm_t) i;
            break;
        }
    }
exit:
    return retval;
}

const char* qos_queue_drop_algorithm_to_string(const qos_queue_drop_algorithm_t algorithm) {
    const char* str_algorithm[] = {
        "RED", "DT", "WRED", "BLUE", "SFQ"
    };

    if(algorithm >= QOS_QUEUE_DROP_ALGORITHM_LAST) {
        return "Unknown";
    }
    return str_algorithm[algorithm];
}

qos_queue_drop_algorithm_t qos_queue_drop_algorithm_from_string(const char* const algorithm) {
    qos_queue_drop_algorithm_t retval = QOS_QUEUE_DROP_ALGORITHM_RED;
    when_str_empty(algorithm, exit);

    for(uint32_t i = 0; i < QOS_QUEUE_DROP_ALGORITHM_LAST; i++) {
        const char* str_algorithm = qos_queue_drop_algorithm_to_string((qos_queue_drop_algorithm_t) i);
        if(!strncmp(str_algorithm, algorithm, strlen(str_algorithm))) {
            retval = (qos_queue_drop_algorithm_t) i;
        }
    }
exit:
    return retval;
}
